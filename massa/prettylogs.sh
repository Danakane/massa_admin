#!/bin/bash
ERROR="[ \033[0;31m\033[1m ERROR \033[0m]"
WARN="[ \033[0;33m\033[1m WARN \033[0m ]"
INFO="[ \033[0;32m\033[1m INFO \033[0m ]"

get_utcnow(){
        local res=$( date -u +'%Y-%m-%d %H:%M:%S' )
        echo "$res"
}

get_trace(){
        local utcnow=$(get_utcnow)
        local log_level="${!1}"
        local res="$utcnow $log_level: $2"
        echo "$res"
}
