#!/bin/bash
source /usr/local/lib/prettylogs.sh
client="/usr/local/bin/massa_client" # edit this line to replace it with your massa client if needed
declare -i min_reserve=100

get_wallet_address(){
    local res=$( "$client" wallet_info | grep "Address" | cut -d " " -f2- )
    echo "$res"
}

get_private_key(){
    local res=$( "$client" wallet_info | grep "Private key" | cut -d " " -f3- )
    echo "$res"
}

get_active_rolls(){
    local res=$( "$client" wallet_info | grep "Active rolls" | cut -d " " -f3- )
    echo "$res"
}

get_final_rolls(){
    local res=$( "$client" wallet_info | grep "Final rolls" | cut -d " " -f3- )
    echo "$res"
}

get_final_balance() {
    local res=$( "$client" wallet_info | grep "Final balance" | cut -d " " -f3- )
    echo "${res%.*}"
}

buy_rolls() {
    local address=$(get_wallet_address)
    local res=$( "$client" buy_rolls "$address" "$1" "0" )
    echo "$res"
}

add_key_to_stake(){
    local key=$( get_private_key )
    local res=$( "$client" node_add_staking_private_keys "$key" )
    echo "$res"
}



main() {
    echo -e $(get_trace "INFO" "Starting auto-staking script")
    local active_rolls=$(get_active_rolls)
    echo -e $(get_trace "INFO" "Number of active rolls: $active_rolls")
    local final_rolls=$(get_final_rolls)
    echo -e $(get_trace "INFO" "Number of final rolls: $final_rolls")
    if (( final_rolls == 0 )); then
        echo -e $(get_trace "WARN" "No final rolls detected")
        local balance=$(get_final_balance)
        echo -e $(get_trace "INFO" "Number of tokens available: $balance")
        local purchase_rolls=$(( balance/100 ))
        if (( purchase_rolls > 0 )); then
            echo -e $(get_trace "INFO" "Purchasing $purchase_rolls rolls...")
            local out=$(buy_rolls "$purchase_rolls")
            echo -e $(get_trace "INFO" "$out")
            sleep 300
            final_rolls=$(get_final_rolls)
            if (( final_rolls != 0 )); then
                echo -e $(get_trace "INFO" "$final_rolls rolls purchased, starting staking...")
                out=$(add_key_to_stake)
                echo -e $(get_trace "INFO" "$out")
            else
                echo -e $(get_trace "ERROR" "Purchase failed")
            fi
        else
            echo -e $( get_trace "ERROR" "Insufficient balance; purchase aborted")
        fi
    else
        local balance=$(get_final_balance)
        echo -e $(get_trace "INFO" "Number of tokens available: $balance")
        local purchase_rolls=$(( (balance - min_reserve)/100 ))
        if (( purchase_rolls > 0 )); then
            echo -e $(get_trace "INFO" "Purchasing $purchase_rolls rolls...")
            local out=$(buy_rolls "$purchase_rolls")
            echo -e $(get_trace "INFO" "$out")
            sleep 300
            local total_final_rolls=$(get_final_rolls)
            local bought_rolls=$((total_final_rolls - final_rolls))
            if (( bought_rolls > 0 )); then
                echo -e $(get_trace "INFO" "$bought_rolls rolls purchased, starting staking...")
                out=$(add_key_to_stake)
                echo -e $(get_trace "INFO" "$out")
            else
                echo -e $(get_trace "ERROR" "Purchase failed")
            fi
        else
            echo -e $(get_trace "INFO" "Not enough tokens to make a purchase")
        fi
    fi
    echo -e $(get_trace "INFO" "Terminated.")
}
 
main

