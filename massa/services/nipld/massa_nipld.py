#!/usr/bin/python3

# WARNING: This script will delete comment inside the config file it writes.

import datetime
import configparser
import subprocess
import pathlib
import signal
import threading
import select
import socket
import ipaddress
import random
import time

ERROR="[ \033[0;31m\033[1m ERROR \033[0m]"
WARN="[ \033[0;33m\033[1m WARN \033[0m ]"
INFO="[ \033[0;32m\033[1m INFO \033[0m ]"

TEMPLATE="""
[own]
node=[]

[official]
node_list=[]

[friend]
node_list=[]

[banned]
node_list=[]

[routable]
node_list=[]

[other]
node_list=[]
"""

CLIENT = "/usr/local/bin/massa_client"
DIRECTORY_FILE_PATH = "massa/massa-node/config/directory.toml"
BASE_CONFIG_FILE_PATH = "massa/massa-node/base_config/config.toml"
CONFIG_FILE_PATH = "massa/massa-node/config/config.toml"
CONFIG_FILE_REFRESH_RATE = 900 # 15min
LISTENER_PORT = 48705


class SignalEventTrigger:
    
    def __init__(self, signals, events):
        self.__dict_signum_2_event = dict(zip(signals, events))
        for signum in signals:
            signal.signal(signum, self.catch)

    def catch(self, signum, frame):
        if signum in self.__dict_signum_2_event.keys():
            if not self.__dict_signum_2_event[signum].is_set():
                self.__dict_signum_2_event[signum].set()


class Node:
    
    def __init__(self, serialization="", node_id="", node_ip="", bootstrap_port=0,
                own=False, official=False, friend=False, banned=False, routable=False):
        self.node_id = node_id.strip("\"")
        self.node_ip = node_ip.strip("\"")
        self.own = own
        self.official = official and not self.own
        self.friend = friend and not (self.own or self.official)
        self.banned = banned and not (self.own or self.official or self.friend)
        self.routable = official or friend or banned or routable
        self.bootstrap_port = int(bootstrap_port)
        if serialization:
            splits = serialization.strip(" ").strip("\n").split("/")[0][1:-1].split(", ")
            self.node_id = splits[1].strip("\"")
            bootstrap_addres = ""
            bootstrap_port = 0
            if splits[0].startswith("\"[") :
                self.node_ip = splits[0][2:splits[0].rfind("]")]
                port_substr = splits[0][splits[0].rfind("]") + 2:].strip("\"")
                if port_substr and int(port_substr):
                    self.bootstrap_port = int(port_substr)
                    self.routable = True
            else:
                parts = splits[0].strip("\"").split(":")
                self.node_ip = parts[0]
                if len(parts) > 1 and int(parts[1]):
                    self.bootstrap_port = int(parts[1])
                    self.routable = True
        if (self.official or self.friend or self.banned or self.routable) and not self.bootstrap_port:
            self.bootstrap_port = 31245

    def __eq__(self, other): 
        if not isinstance(other, Node):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.node_id == other.node_id

    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        return hash(self.node_id)

    def serialize(self):
        serialization = ""
        ip_class = None
        try:
            ip_class = ipaddress.ip_address(str(self.node_ip)).__class__
        except ValueError:
            pass
        node_ip = self.node_ip
        if ip_class == ipaddress.IPv6Address:
            node_ip = f"[{node_ip}]"
        if self.routable:
            serialization = f"[\"{node_ip}:{self.bootstrap_port}\", \"{self.node_id}\"]"
        else:
            serialization = f"[\"{node_ip}\", \"{self.node_id}\"]"
        return serialization


class Message:
    def __init__(self, nodes, force_override=False):
        self.nodes = nodes
        self.force_override = force_override


class MassaNodeIPLocalDirectory:

    GET_STATUS_MIN_LINES = 45

    def __init__(self, client, directory_file, base_config_file, config_file, config_file_refresh_rate, port):
        random.seed(9124)
        self.__client = client
        self.__directory_file = directory_file
        self.__base_config_file = base_config_file
        self.__config_file = config_file
        self.__config_file_refresh_rate = config_file_refresh_rate
        self.__port = port
        self.__messages = [] # messages containing the nodes to write/update in directory file 
        self.__lock = threading.RLock() # lock to protect the message list
        self.__stop_event = threading.Event()
        self.__directory_updated_event = threading.Event()
        self.__stop_signal_handler = SignalEventTrigger([signal.SIGTERM, signal.SIGHUP, signal.SIGINT], [self.__stop_event, self.__stop_event, self.__stop_event])

    def __get_trace(self, level, message):
        utcnow = datetime.datetime.utcnow()
        return f"{utcnow} {level}: {message}"

    def __get_neighbour_nodes(self, pattern=""):
        nodes = []
        client_get_status = subprocess.Popen([self.__client, "get_status"], stdout=subprocess.PIPE)
        grep_ip_address = subprocess.Popen(["grep", "IP address:"], stdin=client_get_status.stdout, stdout=subprocess.PIPE)
        grep = None
        if pattern:
            grep = subprocess.Popen(["grep", f"{pattern}"], stdin=grep_ip_address.stdout, stdout=subprocess.PIPE)
        else:
            grep = grep_ip_address
        output, error = grep.communicate()
        if (not error) and output:
            output = output.decode("UTF-8").strip("\n").strip(" ").strip("\t") # trim newlines, spaces and tabs
            for line in output.split("\n"):
                splits = line.strip("\n").strip(" ").strip("\t").split(" ")
                node_id = splits[2]
                node_ip = splits[6]
                routable = True
                if "::ffff:" in node_ip:
                    routable = False
                    node_ip = node_ip.replace("::ffff:", "")
                node = Node(node_id=node_id, node_ip=node_ip, routable=routable)
                nodes.append(node)
        elif error:
            error = error.decode("UTF-8")
            print (self.__get_trace(ERROR, f"Failed to obtain connected nodes: {error}"), flush=True)
        return nodes

    def __get_section_nodes(self, section):
        nodes = []
        parser = configparser.ConfigParser()
        parser.read(self.__directory_file)
        if section != "own":
            lines = parser[section]["node_list"][1:-1].split(",\n")
        else:
            lines = [parser[section]["node"]]
        nodes = []
        if section != "other":
            nodes = [Node(**{"serialization": line.strip(" ").strip("\n"), section: True}) for line in lines if line]
        else:
            nodes = [Node(serialization = line.strip(" ").strip("\n")) for line in lines if line]
        return nodes

    def __get_own_node(self, fetch=False):
        node = None
        if fetch:
            client_get_status = subprocess.Popen([self.__client, "get_status"], stdout=subprocess.PIPE)
            output, error = client_get_status.communicate()
            if (not error) and output:
                lines = output.decode("UTF-8").split("\n")
                if len(lines) >= MassaNodeIPLocalDirectory.GET_STATUS_MIN_LINES:
                    # get the node id
                    node_id = lines[0].split(" ")[-1]
                    node_ip = lines[1].split(" ")[-1]
                    routable = False
                    config_parser = configparser.ConfigParser()
                    config_parser.read(self.__config_file)
                    if "network" in config_parser and "routable_ip" in config_parser["network"]:
                        routable = True
                    bootstrap_port = 0
                    if routable:
                        if "bootstrap" in config_parser and "bind" in config_parser["bootstrap"]:
                            splits = config_parser["bootstrap"]["bind"].split(":")
                            bootstrap_port = int(splits[-1])
                        else:
                            base_config_parser = configparser.ConfigParser(allow_no_value=True)
                            base_config_parser.read(self.__base_config_file)
                            if "bootstrap" in base_config_parser and "bind" in base_config_parser["bootstrap"]:
                                splits = base_config_parser["bootstrap"]["bind"].strip("\"").split(":")
                                bootstrap_port = int(splits[-1])
                            else:
                                print(self.__get_trace(ERROR, f"Unable to find bootstrap port binding in base config file {self.__base_config_file}"))
                    node = Node(node_id=node_id, node_ip=node_ip, own=True, routable=routable, bootstrap_port=bootstrap_port)
                else:
                    print (self.__get_trace(ERROR, f"Invalid get_status output (too few lines): The node has likely crashed"), flush=True)
            elif error:
                error = error.decode("UTF-8")
                print (self.__get_trace(ERROR, f"Failed to obtain connected nodes: {error}"), flush=True)
        else:
            node = self.__get_section_nodes("own")[0]
        return node

    def __get_official_nodes(self, fetch=False):
        nodes = []
        if fetch:
            parser = configparser.ConfigParser(allow_no_value=True) # configparser doesn't support toml format
            parser.read(self.__base_config_file)
            output = ""
            if "bootstrap" in parser and "bootstrap_list" in parser["bootstrap"]:
                output = parser["bootstrap"]["bootstrap_list"][2:] # [2:] to remove the first "[\n"
                nodes = [Node(serialization=line, official=True) for line in output.split(",\n") if line]
            else:
                print (self.__get_trace(ERROR, f"Unable to find official bootstrappers in base config file {base_config_file}"), flush=True)
        else:
            nodes = self.__get_section_nodes("official")
        return nodes

    def __get_friend_nodes(self):
        return self.__get_section_nodes("friend")

    def __get_banned_nodes(self):
        return self.__get_section_nodes("banned")

    def __get_routable_nodes(self):
        return self.__get_section_nodes("routable")

    def __get_other_nodes(self):
        return self.__get_section_nodes("other")

    def __get_nodes(self, patterns):
        nodes = []
        patterns = patterns.strip(" ").strip("\n").split(" ")
        search_patterns = []
        for pattern in patterns:
            if pattern == "own":
                nodes.append(self.__get_own_node())
            elif "official" == pattern:
                nodes += self.__get_official_nodes()
            elif "friend" == pattern:
                nodes += self.__get_friend_nodes()
            elif "banned" == pattern:
                nodes += self.__get_banned_nodes()
            elif "routable" == pattern:
                nodes += self.__get_routable_nodes()
            elif "other" == pattern:
                nodes += self.__get_other_nodes()
            else:
                search_patterns.append(pattern)
        if search_patterns:
            directory_nodes = [self.__get_own_node()]
            directory_nodes += self.__get_official_nodes()
            directory_nodes += self.__get_friend_nodes()
            directory_nodes += self.__get_banned_nodes()
            directory_nodes += self.__get_routable_nodes()
            directory_nodes += self.__get_other_nodes()
            for pattern in search_patterns:
                for node in directory_nodes:
                    if node.node_id == pattern or node.node_ip == pattern:
                        nodes.append(node)
        return nodes

    def __get_faulty_bootstrappers(self):
        # edit the following line to change service name or if you don't use systemd
        # for example if you don't use systemd: journalctl = subprocess.Popen(["cat", "path_to_log_file"], stdout=subprocess.PIPE)
        bootstrappers = []
        routable_nodes = self.__get_routable_nodes()
        journalctl = subprocess.Popen(["journalctl", "-u", "massa", "--since", "1 hour ago", "-q"], stdout=subprocess.PIPE)
        grep = subprocess.Popen(["grep", "-B", "1", "error while bootstrapping"], stdin=journalctl.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = grep.communicate()
        if not error:
            logs = output.decode("UTF-8").split("\n")
            for line in logs:
                if "Start bootstrapping from" in line:
                    bootstrap_address = line.split(" ")[-1].split(":")
                    node_ip = ":".join(bootstrap_address[0:-1]).strip("[").strip("]")
                    port = int(bootstrap_address[-1])
                    for node in routable_nodes:
                        if node.node_ip == node_ip and node.bootstrap_port == port:
                            print (self.__get_trace(WARN, f"Removing node {node.serialize()} from [routable] nodes for bootstrap failure"), flush=True)
                            node.routable = False
                            bootstrappers.append(node)
        else:
            error = error.decode("UTF-8")
            print (self.__get_trace(ERROR, f"Failed to proceed logs: unable to remove faulty bootstrappers: {error}"), flush=True)
        return bootstrappers

    def __create_or_repair_directory_file(self):
        parser = configparser.ConfigParser()
        if not pathlib.Path(self.__directory_file).is_file():
            # create directory file
            parser.read_string(TEMPLATE)
        else:
            parser.read(self.__directory_file)
        if not "own" in parser:
            parser["own"] = {}
        if not "official" in parser:
            parser["official"] = {}
        if not "friend" in parser:
            parser["friend"] = {}
        if not "banned" in parser:
            parser["banned"] ={}
        if not "routable" in parser:
            parser["routable"] = {}
        if not "other" in parser:
            parser["other"] = {}
        if not "node" in parser["own"]:
            own_node = self.__get_own_node(fetch=True)
            serialization = own_node.serialize()
            parser["own"]["node"] = f"{serialization}" 
        if not "node_list" in parser["official"]:
            official_nodes = self.__get_official_nodes(fetch=True)
            serialization = ",\n".join(node.serialize() for node in official_nodes)
            parser["official"]["node_list"] = f"[{serialization}]" 
        if not "node_list" in parser["friend"]:
            parser["friend"]["node_list"] = "[]"
        if not "node_list" in parser["banned"]:
            parser["banned"]["node_list"] = "[]"
        if not "node_list" in parser["routable"]:
            parser["routable"]["node_list"] = "[]"
        if not "node_list" in parser["other"]:
            parser["other"]["node_list"] = "[]"
        with open(self.__directory_file, "w") as file:
            parser.write(file)
                
    def __update_directory_file(self, new_nodes, force_override=False):
        print (self.__get_trace(INFO, f"Updating massa nodes directory file {self.__directory_file}"), flush=True)
        parser = configparser.ConfigParser()
        parser.read_string(TEMPLATE)
        # fetch own node
        own_node = self.__get_own_node(fetch=True)
        # fetch official nodes
        official_nodes = self.__get_official_nodes(fetch=True)
        # get friend nodes
        friend_nodes = self.__get_friend_nodes()
        # get banned nodes
        banned_nodes = self.__get_banned_nodes()
        # get routable nodes
        routable_nodes = self.__get_routable_nodes()
        # get other nodes
        other_nodes = self.__get_other_nodes()
        # filter duplicates
        directory_nodes = [own_node]
        for nodes in [official_nodes, friend_nodes, banned_nodes, routable_nodes, other_nodes]:
            for node in nodes:
                if not node in directory_nodes:
                    directory_nodes.append(node)
        for node in new_nodes:
            if not node in directory_nodes:
                directory_nodes.append(node)
                print (self.__get_trace(INFO, f"Adding new node {node.serialize()} to directory file"), flush=True)
            else: 
                # update the node if it's routable (case a non routable node happen to be routable)
                # or if it's IP address changed
                # or if we force it (case a routable fail to bootstrap our node, it becomes non-routable). 
                node_to_update = directory_nodes[directory_nodes.index(node)]
                if node_to_update.node_ip != node.node_ip:
                    print (self.__get_trace(INFO, f"Updating node {node_to_update.serialize()} IP address to {node.node_ip} in directory file"), flush=True)
                    node_to_update.node_ip = node.node_ip
                if force_override or (node.routable and not node_to_update.routable):
                    print (self.__get_trace(INFO, f"Updating node {node_to_update.serialize()} routable status in directory file"), flush=True)
                    node_to_update.routable = node.routable
                    trace = f"Node {node_to_update.serialize()} is now considered as routable" \
                        if node_to_update.routable else f"Node {node_to_update.serialize()} is now considered as non-routable"
                    print (self.__get_trace(INFO, trace), flush=True)
                else:
                    print (self.__get_trace(INFO, f"Node {node_to_update.serialize()} is already registered and up to date"), flush=True)
        # set own node to parser
        parser["own"]["node"] = own_node.serialize()
        # set offcial nodes to parser
        serialization = ",\n".join([node.serialize() for node in directory_nodes if node.official])
        parser["official"]["node_list"] = f"[{serialization}]"
        # set friend nodes to parser
        serialization = ",\n".join([node.serialize() for node in directory_nodes if node.friend])
        parser["friend"]["node_list"] = f"[{serialization}]"
        # set banned nodes to parser
        serialization = ",\n".join([node.serialize() for node in directory_nodes if node.banned])
        parser["banned"]["node_list"] = f"[{serialization}]"
        # set routable nodes to parser
        serialization = ",\n".join([node.serialize() for node in directory_nodes 
            if (node.routable and not node.own and not node.official and not node.friend and not node.banned)])
        parser["routable"]["node_list"] = f"[{serialization}]"
        # set other nodes to parser
        serialization = ",\n".join([node.serialize() for node in directory_nodes if not (node.own or node.routable) ])
        parser["other"]["node_list"] = f"[{serialization}]"
        # write the file
        with open(self.__directory_file, "w") as file:
            parser.write(file)
        print (self.__get_trace(INFO, f"Massa nodes directory file update done."), flush=True)

    def __update_config_file(self):
        print (self.__get_trace(INFO, f"Updating massa config file {self.__config_file}"), flush=True)
        # create the bootstrap list
        bootstrappers = list(set(self.__get_official_nodes() + self.__get_friend_nodes() + self.__get_routable_nodes()))
        print(self.__get_trace(INFO, "Randomizing the boostrap list order"), flush=True)
        random.shuffle(bootstrappers)
        bootstrap_list = ",\n".join([bootstrapper.serialize() for bootstrapper in bootstrappers])
        # create config file if it doesn't exist
        open(self.__config_file, "a").close()
        parser = configparser.ConfigParser()
        parser.read(self.__config_file)
        if not "bootstrap" in parser:
            parser["bootstrap"] = {}
        parser["bootstrap"]["bootstrap_list"] = f"[{bootstrap_list}]"
        with open(self.__config_file, "w") as file:
            parser.write(file)
        print (self.__get_trace(INFO, f"Massa config file update done."), flush=True)

    def __do_collect_nodes_from_logs(self):
        # tail massa logs with journalctl
        print(self.__get_trace(INFO, "Massa logs parser thread has started"), flush=True)
        args = ["journalctl", "-u", "massa", "-q", "-f", "-n", "0"]
        journalctl = subprocess.Popen(args, stdout=subprocess.PIPE)
        p = select.poll()
        p.register(journalctl.stdout)
        while not self.__stop_event.is_set():
            if p.poll(100):
                time.sleep(0.1) # sometimes the fd is actually not ready for read, sleep 100ms to give it more time...
                trace = journalctl.stdout.readline()
                if trace:
                    trace = trace.decode("UTF-8")
                    if "massa_protocol_worker::protocol_worker: Connected to node" in trace:
                        node_id = trace.split(" ")[-1].strip(" ").strip("\n")
                        print(self.__get_trace(INFO, f"New connection with node {node_id} detected"), flush=True)
                        time.sleep(0.2) # sometimes get_status output isn't up to date, sleep 200ms to give it more time...
                        nodes = self.__get_neighbour_nodes(pattern=node_id)
                        if nodes:
                            print(self.__get_trace(INFO, f"Corresponding node found with get_status"), flush=True)
                            with self.__lock:
                                self.__messages.append(Message(nodes))
                        else:
                            print(self.__get_trace(WARN, f"Node {node_id} not found with get_status"), flush=True)
            else:
                time.sleep(0.1) # nothing to read just sleep a bit
        print(self.__get_trace(INFO, "Massa logs parser thread terminated gracefully"), flush=True)

    def __do_update_directory_file(self):
        print(self.__get_trace(INFO, "Directory file updater thread has started"), flush=True)
        # can have loss if stop is triggered while the list isn't empty, should a rare edge case
        while not self.__stop_event.is_set():
            if self.__messages:
                with self.__lock:
                    for message in self.__messages:
                        self.__update_directory_file(message.nodes, message.force_override)
                    self.__messages.clear()
                    if not self.__directory_updated_event.is_set():
                        self.__directory_updated_event.set()
            self.__stop_event.wait(timeout=0.1) # wait 100ms or the stop event
        print(self.__get_trace(INFO, "Directory file updater thread terminated gracefully"), flush=True)

    def __do_update_config_file(self):
        print(self.__get_trace(INFO, "Config file updater thread has started"), flush=True)
        while not self.__stop_event.is_set():
            nodes = self.__get_faulty_bootstrappers()
            if nodes:
                with self.__lock:
                    self.__messages.append(Message(nodes,force_override=True))
                self.__directory_updated_event.wait(timeout=5)
                self.__directory_updated_event.clear()
            self.__update_config_file()
            self.__stop_event.wait(timeout=self.__config_file_refresh_rate) # wait 15min or the stop event
        print(self.__get_trace(INFO, "Config file updater thread terminated gracefully"), flush=True)

    def __do_handle_requests(self):
        print(self.__get_trace(INFO, "Clients handler thread has started"), flush=True)
        sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) # udp socket
        sock.bind(("localhost", self.__port)) # listen on localhost only
        sock.setblocking(0) # non-blocking
        while not self.__stop_event.is_set():
            try:
                patterns, client = sock.recvfrom(4096)
                if patterns and client:
                    patterns = patterns.decode("UTF-8")
                    print (self.__get_trace(INFO, f"Received request '{patterns}'"), flush=True)
                    nodes = self.__get_nodes(patterns)
                    sock.sendto(str(len(nodes)).encode(), client)
                    for node in nodes:
                        sock.sendto(node.serialize().encode(), client)
            except BlockingIOError:
                time.sleep(0.1) # nothing to read just sleep a bit
        sock.close()
        print(self.__get_trace(INFO, "Client handle thread terminated gracefully"), flush=True)

    def run(self):
        # create/repair directory file if needed:
        self.__create_or_repair_directory_file()
        # get neighbours_nodes  
        neighbour_nodes = self.__get_neighbour_nodes()
        # update the directory file
        self.__update_directory_file(neighbour_nodes)
        # start the threads:
        print(self.__get_trace(INFO, "Starting directory file updater thread"), flush=True)
        th_update_directory = threading.Thread(target=self.__do_update_directory_file)
        th_update_directory.start()
        print(self.__get_trace(INFO, "Starting massa logs parser thread"), flush=True)
        th_collect_nodes_from_logs = threading.Thread(target=self.__do_collect_nodes_from_logs)
        th_collect_nodes_from_logs.start()
        print(self.__get_trace(INFO, "Starting config file updater thread"), flush=True)
        th_update_config_file = threading.Thread(target=self.__do_update_config_file)
        th_update_config_file.start()
        print(self.__get_trace(INFO, "Starting client handle thread"), flush=True)
        th_handle_requests = threading.Thread(target=self.__do_handle_requests)
        th_handle_requests.start()
        # join threads
        th_update_directory.join()
        th_collect_nodes_from_logs.join()
        th_update_config_file.join()
        th_handle_requests.join()
        print(self.__get_trace(INFO, "massa_nipld terminated successfully"), flush=True)


if __name__ == "__main__":
    massa_nipld_agent = MassaNodeIPLocalDirectory(CLIENT, DIRECTORY_FILE_PATH, BASE_CONFIG_FILE_PATH, CONFIG_FILE_PATH, CONFIG_FILE_REFRESH_RATE, LISTENER_PORT)
    massa_nipld_agent.run()

