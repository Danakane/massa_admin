#!/bin/bash

ERROR="[ \033[0;31m\033[1m ERROR \033[0m]"
WARN="[ \033[0;33m\033[1m WARN \033[0m ]"
OK="[ \033[0;32m\033[1m  OK  \033[0m ]"

get_trace(){
        local log_level="${!1}"
        local res="$log_level: $2"
        echo "$res"
}

main(){
    if [[ "$(whoami)" == "root" ]]; then
        # get user's name
        local user
        read -p "Please enter the name of the user who run massa node: " user

        # get node public ip
        local node_ip
        read -p "Please enter your node public IP address (leave blank if the node is not routable): " node_ip

        # deploy config.toml
        local massa_directory
        if [[ "$user" == "root" ]]; then
            massa_directory="/root/massa"
        else
            massa_directory="/home/$user/massa"
        fi
        if [[ ! -d "$massa_directory/massa-node/config" ]]; then
            $(su "$user" -c "mkdir $massa_directory/massa-node/config")
            echo -e $(get_trace OK "directory $massa_directory/massa-node/config created")
        fi
        $(su "$user" -c "cp massa/config/config.toml $massa_directory/massa-node/config/")
        if [ -z "$node_ip" ]; then
            $(sed -i "/network/d" "$massa_directory/massa-node/config/config.toml")
            $(sed -i "/replace/d" "$massa_directory/massa-node/config/config.toml")
            $(sed -i "/routable_ip/d" "$massa_directory/massa-node/config/config.toml")
            $(sed -i "/connections/d" "$massa_directory/massa-node/config/config.toml")
        else
            $(sed -i "s/your_ip/$node_ip/g" "$massa_directory/massa-node/config/config.toml")
        fi

        # deploy prettylogs.sh
        $(cp massa/prettylogs.sh /usr/local/lib/)
        $(chmod u+rw /usr/local/lib/prettylogs.sh)
        $(chmod go+r /usr/local/lib/prettylogs.sh)
        echo -e $(get_trace OK "prettylogs.sh deployed in /usr/local/lib")

        # deploy massa_client
        $(su "$user" -c "cp massa/massa_client tmp")
        $(mv tmp /usr/local/bin/massa_client)
        $(sed -i "s/your_user_name/$user/g" /usr/local/bin/massa_client)
        $(su "$user" -c "chmod u+rwx /usr/local/bin/massa_client")
        $(su "$user" -c "chmod go-rwx /usr/local/bin/massa_client")
        echo -e $(get_trace OK "massa_client deployed in /usr/local/bin")

        # deploy build_massa_client
        $(cp massa/build/build_massa_client /usr/local/bin/)
        $(sed -i "s/your_user_name/$user/g" /usr/local/bin/build_massa_client)
        $(chmod u+rwx /usr/local/bin/build_massa_client)
        $(chmod go+rx /usr/local/bin/build_massa_client)
        echo -e $(get_trace OK "build_massa_client deployed in /usr/local/bin")

        # deploy build_massa_node
        $(cp massa/build/build_massa_node /usr/local/bin/)
        $(sed -i "s/your_user_name/$user/g" /usr/local/bin/build_massa_node)
        $(chmod u+rwx /usr/local/bin/build_massa_node)
        $(chmod go+rx /usr/local/bin/build_massa_node)
        echo -e $(get_trace OK "build_massa_node deployed in /usr/local/bin")

        # deploy massa_logger
        $(cp massa/massa_logger /usr/local/bin/)
        $(chmod u+rwx /usr/local/bin/massa_logger)
        $(chmod go+rx /usr/local/bin/massa_logger)
        echo -e $(get_trace OK "build_massa_logger deployed in /usr/local/bin")

        # deploy massa.service
        $(cp massa/services/massa.service /etc/systemd/system/)
        $(sed -i "s/your_user_name/$user/g" /etc/systemd/system/massa.service)
        echo -e $(get_trace OK "massa.service deployed in /etc/systemd/system")

        # deploy massa_watchdog.sh
        $(cp massa/services/watchdog/massa_watchdog.sh /usr/local/bin/)
        $(sed -i "s/your_user_name/$user/g" /usr/local/bin/massa_watchdog.sh)
        $(chmod u+rwx /usr/local/bin/massa_watchdog.sh)
        $(chmod go-rwx /usr/local/bin/massa_watchdog.sh)
        echo -e $(get_trace OK "massa_watchdog.sh deployed in /usr/local/bin")
        # deploy massa_watchdog.service
        $(cp massa/services/watchdog/massa_watchdog.service /etc/systemd/system/)
        echo -e $(get_trace OK "massa_watchdog.service deployed in /etc/systemd/system")
        # deploy massa_watchdog.timer
        $(cp massa/services/watchdog/massa_watchdog.timer /etc/systemd/system/)
        echo -e $(get_trace OK "massa_watchdog.timer deployed in /etc/systemd/system")

        # deploy massa_auto_staking.sh
        $(cp massa/services/auto_staking/massa_auto_staking.sh /usr/local/bin/)
        $(chmod u+rwx /usr/local/bin/massa_auto_staking.sh)
        $(chmod go+rx /usr/local/bin/massa_auto_staking.sh)
        echo -e $(get_trace OK "massa_auto_staking.sh deployed in /usr/local/bin")
        # deploy massa_auto_staking.service
        $(cp massa/services/auto_staking/massa_auto_staking.service /etc/systemd/system/)
        $(sed -i "s/your_user_name/$user/g" /etc/systemd/system//massa_auto_staking.service)
        echo -e $(get_trace OK "massa_auto_staking.service deployed in /etc/systemd/system")
        # deploy massa_auto_staking.timer
        $(cp massa/services/auto_staking/massa_auto_staking.timer /etc/systemd/system/)
        echo -e $(get_trace OK "massa_auto_staking.timer deployed in /etc/systemd/system")

        # deploy massa_nipld.py
        local port
        read -p "Please enter a port number for massa_nipld service (the service will only be accessible on localhost interface. Default: 48705): " port
        $(su "$user" -c "cp massa/services/nipld/massa_nipld.py tmp")
        $(mv tmp /usr/local/bin/massa_nipld.py)
        $(su "$user" -c "chmod u+rwx /usr/local/bin/massa_nipld.py")
        $(su "$user" -c "chmod go-rwx /usr/local/bin/massa_nipld.py")
        if [ ! -z "$port" ]; then
            $(sed -i "s/48705/$port/g" /usr/local/bin/massa_nipld.py)
        fi
        echo -e $(get_trace OK "massa_nipld.py deployed in /usr/local/bin")
        # deploy massa_whois
        $(su "$user" -c "cp massa/services/nipld/massa_whois tmp")
        $(mv tmp /usr/local/bin/massa_whois)
        $(su "$user" -c "chmod u+rwx /usr/local/bin/massa_whois")
        $(su "$user" -c "chmod go-rwx /usr/local/bin/massa_whois")
        if [ ! -z "$port" ]; then
            $(sed -i "s/48705/$port/g" /usr/local/bin/massa_whois)
        fi
        echo -e $(get_trace OK "massa_whois deployed in /usr/local/bin")
        # deploy massa_nipld.service
        $(cp massa/services/nipld/massa_nipld.service /etc/systemd/system/)
        $(sed -i "s/your_user_name/$user/g" /etc/systemd/system/massa_nipld.service)
        echo -e $(get_trace OK "massa_nipld.service deployed in /etc/systemd/system")

        # ask if user want to install massa_faucet_spammer
        local deploy_faucet_spammer
        read -p "Do you want to use massa_faucet_spammer (y/n): " deploy_faucet_spammer
        if [ ${deploy_faucet_spammer,,} = "y" ]; then
            local discord_token
            read -p "Please enter your token (WARNING: it's highly recommended to NOT use your main discord account for this as it may cause Discord to ban your account): " discord_token
            # intall discord.py
            python3 -m pip install -U discord.py
            # deploy massa_faucet_spammer.py
            $(su "$user" -c "cp massa/services/faucet_spammer/massa_faucet_spammer.py tmp")
            $(mv tmp /usr/local/bin/massa_faucet_spammer.py)
            $(sed -i "s/Insert your discord account token/$discord_token/g" /usr/local/bin/massa_faucet_spammer.py)
            $(su "$user" -c "chmod u+rwx /usr/local/bin/massa_faucet_spammer.py")
            $(su "$user" -c "chmod go-rwx /usr/local/bin/massa_faucet_spammer.py")
            echo -e $(get_trace OK "massa_faucet_spammer.py deployed in /usr/local/bin")
            # deploy massa_faucet_spammer.service
            $(cp massa/services/faucet_spammer/massa_faucet_spammer.service /etc/systemd/system/)
            $(sed -i "s/your_user_name/$user/g" /etc/systemd/system/massa_faucet_spammer.service)
            echo -e $(get_trace OK "massa_faucet_spammer.service deployed in /etc/systemd/system")
            # deploy massa_faucet_spammer.timer
            $(cp massa/services/faucet_spammer/massa_faucet_spammer.timer /etc/systemd/system/)
            echo -e $(get_trace OK "massa_faucet_spammer.timer deployed in /etc/systemd/system")
        fi

        local reload_daemons
        read -p "Reload systemd's daemons (y/n): " reload_daemons
        if [ ${reload_daemons,,} = "y" ]; then
            $(systemctl daemon-reload)
            echo -e $(get_trace OK "systemd's daemons reloaded")
        fi
        echo -e $(get_trace OK "massa_admin installation completed")
    else
        echo -e $(get_trace ERROR "Install aborted: massa_admin setup.sh needs root permissions.")
    fi
}

main
